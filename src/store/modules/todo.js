import Vue from 'vue'

const state = {
  todos: [],
  loading: true,
  filter: 'all'
}

const getters = {
  isLoading (state) {
    return state.loading
  },
  filteredTodos (state) {
    if (state.filter === 'todo') {
      return state.todos.filter(todo => !todo.isDone)
    } else if (state.filter === 'done') {
      return state.todos.filter(todo => todo.isDone)
    }
    return state.todos
  },
  remaining () {
    return state.todos.filter(todo => !todo.isDone).length
  },
  todos () {
    return state.todos
  },
  filter () {
    return state.filter
  }
}

const actions = {
  getTodos ({commit}) {
    commit('setLoading', true)
    Vue.http.get('http://localhost:8000/api/todos').then(response => {
      commit('loadTodos', response.data['hydra:member'])
    }, response => {
      console.log(response)
    }).then(_ => {
      commit('setLoading', false)
    })
  },

  removeTodo ({state, commit}, todo) {
    commit('setLoading', true)
    Vue.http.delete('http://localhost:8000/api/todos/' + todo.id).then(response => {
      commit('loadTodos', state.todos.filter(_todo => _todo.id !== todo.id))
    }, response => {
      console.log(response)
    }).then(_ => {
      commit('setLoading', false)
    })
  },

  addTodo ({state, commit}, newTodo) {
    commit('setLoading', true)
    Vue.http.post('http://localhost:8000/api/todos', {name: newTodo}).then(response => {
      commit('pushTodo', response.data)
      newTodo = ''
    }, response => {
      return false
    }).then(_ => {
      commit('setLoading', false)
    })
    return true
  },

  updateTodo ({state, commit}, todo) {
    Vue.http.put('http://localhost:8000/api/todos/' + todo.id, todo).then(response => {
    }, response => {
      console.log(response)
    })
  },

  removeCompleted ({dispatch}) {
    state.todos.forEach(todo => {
      if (todo.isDone) {
        dispatch('removeTodo', todo)
      }
    })
  }
}

const mutations = {
  loadTodos (state, todos) {
    state.todos = todos
  },
  pushTodo (state, todo) {
    state.todos.push(todo)
  },
  setLoading (state, loading) {
    state.loading = loading
  },
  setFilter (state, filter) {
    state.filter = filter
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
