import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Notes from '@/components/Notes'
import Todo from '@/components/Todo'
import NoteDetail from '@/components/NoteDetail'
import NoteCreate from '@/components/NoteCreate'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'Home',
      component: Home
    },
    {
      path: '/notes',
      name: 'Notes',
      component: Notes,
      children: [
        { name: 'noteDetail', path: 'detail/:id(\\d+)', component: NoteDetail },
        { name: 'noteCreate', path: 'new', component: NoteCreate }
      ]
    },
    {
      path: '/todo',
      name: 'Todo',
      component: Todo
    }
  ]
})
